<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Clothe;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Clothe controller.
 *
 * @Route("clothe")
 */
class ClotheController extends Controller
{
    /**
     * Lists all clothe entities.
     *
     * @Route("/", name="clothe_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $clothes = $em->getRepository('AppBundle:Clothe')->findAll();

        return $this->render('clothe/index.html.twig', array(
            'clothes' => $clothes,
        ));
    }

    /**
     * Creates a new clothe entity.
     *
     * @Route("/new", name="clothe_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $clothe = new Clothe();
        $form = $this->createForm('AppBundle\Form\ClotheType', $clothe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($clothe);
            $em->flush();

            return $this->redirectToRoute('clothe_show', array('id' => $clothe->getId()));
        }

        return $this->render('clothe/new.html.twig', array(
            'clothe' => $clothe,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a clothe entity.
     *
     * @Route("/{id}", name="clothe_show")
     * @Method("GET")
     */
    public function showAction(Clothe $clothe)
    {
        $deleteForm = $this->createDeleteForm($clothe);

        return $this->render('clothe/show.html.twig', array(
            'clothe' => $clothe,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing clothe entity.
     *
     * @Route("/{id}/edit", name="clothe_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Clothe $clothe)
    {
        $deleteForm = $this->createDeleteForm($clothe);
        $editForm = $this->createForm('AppBundle\Form\ClotheType', $clothe);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('clothe_edit', array('id' => $clothe->getId()));
        }

        return $this->render('clothe/edit.html.twig', array(
            'clothe' => $clothe,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a clothe entity.
     *
     * @Route("/{id}", name="clothe_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Clothe $clothe)
    {
        $form = $this->createDeleteForm($clothe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($clothe);
            $em->flush();
        }

        return $this->redirectToRoute('clothe_index');
    }

    /**
     * Creates a form to delete a clothe entity.
     *
     * @param Clothe $clothe The clothe entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Clothe $clothe)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('clothe_delete', array('id' => $clothe->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
